import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AutoresComponent} from './autores/autores.component';
import {CategoriasComponent} from './categorias/categorias.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { InicioComponent } from './inicio/inicio.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent
  },
  {
    path: 'autores',
    component: AutoresComponent
  },
  {
    path: 'categorias',
    component: CategoriasComponent
  },
  {
    path: 'noticias',
    component: NoticiasComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
