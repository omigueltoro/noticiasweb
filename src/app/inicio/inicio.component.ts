import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Noticia } from 'src/models/Noticia';
import { NoticiaService } from '../service/noticia/noticia.service';
import { MenuItem } from 'primeng/api/menuitem';
import { CategoriaService } from '../service/categoria/categoria.service';
import { Categoria } from 'src/models/Categoria';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  viewMode;
  items: MenuItem[];
  noticias: Noticia [];
  categorias : Categoria[];
  htmlCategorias : string;

  constructor(private NoticiaService: NoticiaService,private CategoriaService: CategoriaService) { }

  ngOnInit(): void {
    this.getCategorias();
  }
  getCategorias() {
    this.CategoriaService.getAll().subscribe(
      (result: any) => {
        let items: MenuItem[] = [];
        let item : MenuItem = {
          label : 'Ultimas noticias',
          command : () => this.mostrarNoticias('ultimas')
        };
        items.push(item)
        for(let i =0;(i < result.length) && (i < 6); i++){
          let item : MenuItem = {
            label : null,
            command : null
          };
          item.label = result[i].descripcion;
          item.command = () => this.mostrarNoticias(result[i]);
          items.push(item)
        }
        this.items = items;
      },
      error => {
        console.log(error);
      }
    )
  }

  mostrarNoticias(categoria : any){

    if(categoria == 'ultimas'){
      this.NoticiaService.getLast().subscribe( data => {
        this.noticias = data;
  
      });
    }
    else{
      this.NoticiaService.getCategoria(categoria.id).subscribe( data => {debugger;
        this.noticias = data;
  
      });
    }

    
  }
  // getAll() {
  //   this.NoticiaService.getAll().subscribe(
  //     (result: any) => {
  //       let noticias: Noticia[] = [];
  //       for (let i = 0; i < result.length; i++) {
  //         let noticia : Noticia = result[i];
  //         noticia.autor = result[i].autor;
  //         noticia.categoria = result[i].categoria;
  //         noticia.autorCol = result[i].autor.nombres + " " + result[i].autor.apellidos;
  //         noticia.categoriaCol = result[i].categoria.descripcion;
  //         noticia.descripcion = result[i].descripcion;
  //         noticias.push(noticia);
  //       }
  //       this.noticias = noticias;
  //     },
  //     error => {
  //       console.log(error);
  //     }

  //   )
  // }
}
