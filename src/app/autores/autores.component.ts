import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/models/Autor';
import { AutorService } from '../service/autor/autor.service';
import { MenuItem, ConfirmationService } from 'primeng/api';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.css'],
})
export class AutoresComponent implements OnInit {
  
  public title : string = "Autores";
  autores : Autor[];
  cols : any [];
  items: MenuItem [];
  displayAutorSaveDialog : boolean = false;
  autor : Autor = {
    id: null,
    nombres: null,
    apellidos: null,
    cedula: null,
    ciudad: null,
    lugarExpedicion: null,
    activo: true,
    estado: null
  };

  selectedAutor: Autor = {
    id: null,
    nombres: null,
    apellidos: null,
    cedula: null,
    ciudad: null,
    lugarExpedicion: null,
    activo: true,
    estado: null
  };

  constructor(private AutoresService: AutorService, private messageService: MessageService, private confirmService : ConfirmationService) { }

  ngOnInit(): void {
    this.getAll();
    this.cols = [
      {field: "id", header: "Numero"},
      {field: "nombres", header: "Nombres"},
      {field: "apellidos", header: "Apellidos"},
      {field: "cedula", header: "Cedula"},
      {field: "lugarExpedicion", header: "Lugar de expedición"},
      {field: "ciudad", header: "Ciudad de nacimiento"},
      {field: "estado", header: "Estado"},
    ];
    this.items = [
      {
        label : "Registrar",
        icon : "pi pi-fw pi-plus",
        command : () => this.showSaveDialog(false)
      },
      {
        label : "Editar",
        icon : "pi pi-fw pi-pencil",
        command : () => this.selectedAutor.id !=null ? this.showSaveDialog(true) : this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      },
      {
        label : "Eliminar",
        icon : "pi pi-fw pi-times",
        command : () => this.delete()
      }
    ]
  }

  getAll(){
    this.AutoresService.getAll().subscribe(
      (result: any) => {
        let autores : Autor[] = [];
        for(let i =0; i < result.length; i++){
          let autor = result[i] as Autor;
          autor.estado = result[i].activo ? "Activo" : "Inactivo";
          autores.push(autor);
        }
        this.autores = autores;
      },
      error => {
        console.log(error);
      }
      
    )
  }
  showSaveDialog(editar:boolean): void {
    if(editar){
      this.autor = this.selectedAutor;
    }
    else{
      this.autor = {
        id: null,
        nombres: null,
        apellidos: null,
        cedula: null,
        ciudad: null,
        lugarExpedicion: null,
        activo: true,
        estado: null
      };
    }
    this.displayAutorSaveDialog = true;
  }
  save(){
    this.AutoresService.save(this.autor).subscribe(
      (result:any) => {
        let autor = result as Autor;
        autor.estado = result.activo ? "Activo" : "Inactivo";
        let index = this.autores.findIndex(p => p.id == autor.id);
        index != -1 ? this.autores[index] = autor : this.autores.push(autor);
        this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
        this.displayAutorSaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete(){
    if(this.selectedAutor == null || this.selectedAutor.id == null){
      this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      return;
    }
    this.confirmService.confirm({
      message: "Are you sure you want to delete the record?",
      accept : () => {
        this.AutoresService.delete(this.selectedAutor.id).subscribe(
          (result : any) => {
            this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject (id:number) {
    let index = this.autores.findIndex(p => p.id == id);
    if(index != -1){
      this.autores.splice(index, 1)

    }
  }
}
