import { Component, OnInit } from '@angular/core';
import { Noticia } from 'src/models/Noticia';
import { NoticiaService } from '../service/noticia/noticia.service';
import { MenuItem, ConfirmationService, SelectItem } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { CategoriaService } from '../service/categoria/categoria.service';
import { AutorService } from '../service/autor/autor.service';
import { Categoria } from 'src/models/Categoria';
import { Autor } from 'src/models/Autor';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  public title: string = "Noticias";
  noticias: Noticia[];
  cols: any[];
  items: MenuItem[];
  displayNoticiaSaveDialog: boolean = false;
  noticia: Noticia = {
    id: null,
    categoria: null,
    categoriaCol: null,
    autor: null,
    autorCol: null,
    titulo: null,
    descripcion: null
  };
  selectedNoticia: Noticia = {
    id: null,
    categoria: null,
    categoriaCol: null,
    autor: null,
    autorCol: null,
    titulo: null,
    descripcion: null
  };
  public categorias: SelectItem[];
  public autores: SelectItem[];

  constructor(private NoticiaService: NoticiaService, private messageService: MessageService, private confirmService: ConfirmationService,
    private CategoriaService: CategoriaService, private AutorService: AutorService) { }

  ngOnInit(): void {
    var that = this;

    this.getAll();
    this.cols = [
      { field: "id", header: "Item" , width : "10%"},
      { field: "categoriaCol", header: "Categoria" , width : "15%" },
      { field: "autorCol", header: "Autor" , width : "15%" },
      { field: "titulo", header: "Titulo" , width : "20%" },
      { field: "descripcion", header: "Descripción" , width : "40%" }
    ];
    this.items = [
      {
        label: "Registrar",
        icon: "pi pi-fw pi-plus",
        command: () => this.showSaveDialog(false)
      },
      {
        label: "Editar",
        icon: "pi pi-fw pi-pencil",
        command: () => this.selectedNoticia.id != null ? this.showSaveDialog(true) : this.messageService.add({ severity: 'warn', summary: "Result", detail: "Item not selected" })
      },
      {
        label: "Eliminar",
        icon: "pi pi-fw pi-times",
        command: () => this.delete()
      }
    ];
    this.AutorService.getAll().subscribe(
      (result: Autor[]) => {
        if (result) {
          let selectItem: SelectItem [] = []
          for(let i = 0; i < result.length; i++){
            
            let item: SelectItem = { label: result[i].nombres + " " + result[i].apellidos, value: result[i] }
            selectItem.push(item);
          }
          this.autores = selectItem;
        }
      },
      error => {
        console.log(error);
      }
    );
    this.CategoriaService.getAll().subscribe(
      (result: Categoria[]) => {
        if (result) {
          let selectItem: SelectItem [] = []
          for(let i = 0; i < result.length; i++){
            
            let item: SelectItem = { label: result[i].descripcion, value: result[i] }
            selectItem.push(item);
          }
          this.categorias = selectItem;
        }
      },
      error => {
        console.log(error);

      }
    );
  }

  getAll() {
    this.NoticiaService.getAll().subscribe(
      (result: any) => {
        let noticias: Noticia[] = [];
        for (let i = 0; i < result.length; i++) {debugger
          let noticia : Noticia = result[i];
          noticia.autor = result[i].autor;
          noticia.categoria = result[i].categoria;
          noticia.autorCol = result[i].autor.nombres + " " + result[i].autor.apellidos;
          noticia.categoriaCol = result[i].categoria.descripcion;
          noticia.titulo = result[i].titulo ? result[i].titulo.substring(0,100) : "";
          noticia.descripcion = result[i].descripcion ? result[i].descripcion.substring(0,400): "";
          noticias.push(noticia);
        }
        this.noticias = noticias;
      },
      error => {
        console.log(error);
      }

    )
  }
  showSaveDialog(editar: boolean): void {
    if (editar) {
      this.noticia = this.selectedNoticia;
    }
    else {
      this.noticia = {
        id: null,
        categoria: null,
        categoriaCol: null,
        autor: null,
        autorCol: null,
        titulo: null,
        descripcion: null
      };
    }
    this.displayNoticiaSaveDialog = true;
  }
  save() {
    this.NoticiaService.save(this.noticia).subscribe(
      (result: any) => {
        debugger
        let noticia : Noticia = result;
        noticia.categoriaCol = result.categoria.descripcion;
        noticia.autorCol = result.autor.nombres + " " +result.autor.apellidos;
        let index = this.noticias.findIndex(p => p.id == noticia.id);
        index != -1 ? this.noticias[index] = noticia : this.noticias.push(noticia);
        this.messageService.add({ severity: 'success', summary: "Result", detail: "Successful transaction" })
        this.displayNoticiaSaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete() {
    if (this.selectedNoticia == null || this.selectedNoticia.id == null) {
      this.messageService.add({ severity: 'warn', summary: "Result", detail: "Item not selected" })
      return;
    }
    this.confirmService.confirm({
      message: "Are you sure you want to delete the record?",
      accept: () => {
        this.NoticiaService.delete(this.selectedNoticia.id).subscribe(
          (result: any) => {
            this.messageService.add({ severity: 'success', summary: "Result", detail: "Successful transaction" })
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject(id: number) {
    let index = this.noticias.findIndex(p => p.id == id);
    if (index != -1) {
      this.noticias.splice(index, 1)

    }
  }

  onSelectCategoria(categoria: any){
    this.noticia.categoria = categoria;
  }
  onSelectAutor(autor: any){
    this.noticia.autor = autor;
  }
}
