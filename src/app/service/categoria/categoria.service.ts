import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Categoria } from 'src/models/Categoria';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  baseUrl:string = "http://localhost:8080/api/v1"

  constructor(private http:HttpClient) { }

  getAll() : Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
      return this.http.get(this.baseUrl+"/categorias", {headers : headers});
  }
  save(categoria: Categoria): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.baseUrl+"/categorias", JSON.stringify(categoria), {headers : headers});
  }
  delete(id:number) : Observable<any>{

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.delete(this.baseUrl+"/categorias/"+id, {headers : headers});
  }
}
