import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items: MenuItem[];
  title = 'noticiasweb';

  ngOnInit(){
    
    this.items = [
      {label: 'Inicio', icon: 'pi pi-fw pi-home', routerLink: 'inicio'},
      {label: 'Registro de autores', icon: 'pi pi-fw pi-calendar', routerLink: 'autores'},
      {label: 'Registro de categorias', icon: 'pi pi-fw pi-pencil', routerLink: 'categorias'},
      {label: 'Registro de noticas', icon: 'pi pi-fw pi-file', routerLink: 'noticias'}
  ];

  }
}
