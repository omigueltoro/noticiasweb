import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/models/Categoria';
import { CategoriaService } from '../service/categoria/categoria.service';
import { MenuItem, ConfirmationService } from 'primeng/api';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {
  
  public title : string = "Categorias";
  categorias : Categoria[];
  cols : any [];
  items: MenuItem [];
  displayAutorSaveDialog : boolean = false;
  categoria : Categoria = {
    id: null,
    descripcion : null
  };
  selectedCategoria: Categoria = {
    id: null,
    descripcion : null
  }

  constructor(private CategoriasService: CategoriaService, private messageService: MessageService, private confirmService : ConfirmationService) { }

  ngOnInit(): void {
    this.getAll();
    this.cols = [
      {field: "id", header: "Identificador"},
      {field: "descripcion", header: "Categoria"},
    ];
    this.items = [
      {
        label : "Registrar",
        icon : "pi pi-fw pi-plus",
        command : () => this.showSaveDialog(false)
      },
      {
        label : "Editar",
        icon : "pi pi-fw pi-pencil",
        command : () => this.selectedCategoria.id !=null ? this.showSaveDialog(true) : this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      },
      {
        label : "Eliminar",
        icon : "pi pi-fw pi-times",
        command : () => this.delete()
      }
    ]
  }

  getAll(){
    this.CategoriasService.getAll().subscribe(
      (result: any) => {
        let Categorias : Categoria[] = [];
        for(let i =0; i < result.length; i++){
          let categoria = result[i] as Categoria;
          Categorias.push(categoria);
        }
        this.categorias = Categorias;
      },
      error => {
        console.log(error);
      }
    )
  }
  showSaveDialog(editar:boolean): void {
    if(editar){
      this.categoria = this.selectedCategoria;
    }
    else{
      this.categoria = {
        id: null,
        descripcion : null
      };
    }
    this.displayAutorSaveDialog = true;
  }
  save(){
    this.CategoriasService.save(this.categoria).subscribe(
      (result:any) => {
        let categoria = result as Categoria;
        let index = this.categorias.findIndex(p => p.id == categoria.id);
        index != -1 ? this.categorias[index] = categoria : this.categorias.push(categoria);
        this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
        this.displayAutorSaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete(){
    if(this.selectedCategoria == null || this.selectedCategoria.id == null){
      this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      return;
    }
    this.confirmService.confirm({
      message: "Are you sure you want to delete the record?",
      accept : () => {
        this.CategoriasService.delete(this.selectedCategoria.id).subscribe(
          (result : any) => {
            this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject (id:number) {
    let index = this.categorias.findIndex(p => p.id == id);
    if(index != -1){
      this.categorias.splice(index, 1)

    }
  }
}
