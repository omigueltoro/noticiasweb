import { Autor } from './Autor';
import { Categoria } from './Categoria';

export class Noticia{
    id:number;
    categoria: Categoria;
    autor: Autor;
    titulo: string;
    descripcion: string;
    categoriaCol: string;
    autorCol: string;
    constructor(id:number = null,categoria:Categoria = null,autor:Autor = null, descripcion:string = null,
                titulo:string = null){
        
    }
}